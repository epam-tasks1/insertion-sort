﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace InsertionSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with insertion sort algorithm.
        /// </summary>
        public static void InsertionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 1; i < array.Length; i++)
            {
                for (int j = i; j > 0 && array[j - 1] > array[j]; j--)
                {
                    (array[j - 1], array[j]) = (array[j], array[j - 1]);
                }
            }
        }

        /// <summary>
        /// Sorts an <paramref name="array"/> with recursive insertion sort algorithm.
        /// </summary>
        public static void RecursiveInsertionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            RecursiveInsertionSort(array, array.Length);
        }

        public static void RecursiveInsertionSort(this int[] array, int length)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (length <= 1)
            {
                return;
            }

            RecursiveInsertionSort(array, length - 1);

            int value = array[length - 1];
            int index = length - 1;

            RecursiveInsertionSwap(array, index, value);
        }

        public static void RecursiveInsertionSwap(this int[] array, int index, int value)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (index > 0 && array[index - 1] > value)
            {
                (array[index], array[index - 1]) = (array[index - 1], array[index]);
                index -= 1;
                RecursiveInsertionSwap(array, index, value);
            }
        }
    }
}
